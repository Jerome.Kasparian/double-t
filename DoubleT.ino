/*********************

Example code for the Adafruit RGB Character LCD Shield and Library

This code displays text on the shield, and also reads the buttons on the keypad.
When a button is pressed, the backlight changes color.

**********************/

// Libraries for the Adafruit RGB/LCD Shield
#include <Wire.h>
//#include <Adafruit_MCP23017.h>
#include <Adafruit_RGBLCDShield.h>


// Libraries for the DS18B20 Temperature Sensor
#include <OneWire.h>
#include <DallasTemperature.h>

// ************************************************
// Pin definitions
// ************************************************

// One-Wire Temperature Sensor
// (Use GPIO pins for power/ground to simplify the wiring)
#define ONE_WIRE_BUS A0 // jaune
// #define ONE_WIRE_PWR 3 // rouge
// #define ONE_WIRE_GND 4 // bleu
//#define PIN_BUZZER 9
#define PIN_LED 13
#define DEBUG false

// adresses unique des appareils. Cf fonction   discoverOneWireDevices() pour la déterminer
#define TEMP_SENSOR1 {0x28, 0x13, 0x97, 0x9A, 0x0A, 0x00, 0x00, 0x6A}
#define TEMP_SENSOR2 {0x28, 0xFF, 0xE4, 0x9A, 0x0A, 0x00, 0x00, 0x1D}

// The shield uses the I2C SCL and SDA pins. On classic Arduinos
// this is Analog 4 and 5 so you can't use those for analogRead() anymore
// However, you can connect other I2C sensors to the I2C bus and share
// the I2C bus.
Adafruit_RGBLCDShield lcd = Adafruit_RGBLCDShield();

// These #defines make it easy to set the backlight color
//#define RED 0x1
//#define YELLOW 0x3
//#define GREEN 0x2
//#define TEAL 0x6
//#define BLUE 0x4
//#define VIOLET 0x5
#define WHITE 0x7
#define OFFSET1 0 // Surestimation de la temperature par la sonde par rapport a mon thermometre
#define OFFSET2 0 // Surestimation de la temperature par la sonde par rapport a mon thermometre

//unsigned long lastInput = 0; // last button press

byte degree[8] = // define the degree symbol 
{ 
 B00110, 
 B01001, 
 B01001, 
 B00110, 
 B00000,
 B00000, 
 B00000, 
 B00000 
}; 

float temperature1;
float temperature2;
int compteur = 0;
// ************************************************
// Sensor Variables and constants
// Data wire is plugged into port 2 on the Arduino

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature. 
DallasTemperature sensors(&oneWire);

// arrays to hold device address
DeviceAddress tempSensor1 = TEMP_SENSOR1;
DeviceAddress tempSensor2 = TEMP_SENSOR2;


void setup() {
  
  int time = millis();
//  pinMode(PIN_BUZZER, OUTPUT);
  pinMode(PIN_LED, OUTPUT); 

     // Set up Ground & Power for the sensor from GPIO pins
  //  pinMode(ONE_WIRE_GND, OUTPUT);
  //  digitalWrite(ONE_WIRE_GND, LOW);
  //  pinMode(ONE_WIRE_PWR, OUTPUT);
  //  digitalWrite(ONE_WIRE_PWR, HIGH);  
   
   // Initialize LCD DiSplay 
   lcd.begin(16, 2);
   lcd.createChar(1, degree); // create degree symbol from the binary
   lcd.setBacklight(WHITE);

// pour détecter les capteurs connectés à l'Arduino
//  discoverOneWireDevices();

   // Start up the DS18B20 One Wire Temperature Sensor
   sensors.begin();
   sensors.setResolution(tempSensor1, 12);
   sensors.setResolution(tempSensor2, 12);
   sensors.setWaitForConversion(false);
   sensors.requestTemperatures(); // Dit au senseur de mesurer la temérature
    Serial.begin(9600);
   delay(100);
}

void loop() {
  compteur = compteur + 1;
  char texte[16];
  char temp[8];
    temperature1 = sensors.getTempC(tempSensor1) + OFFSET1;
    temperature2 = sensors.getTempC(tempSensor2) + OFFSET2;
    sensors.requestTemperatures(); // prime the pump for the next one - but don't wait
   // if ((compteur % 10) == 0) {  // On n'écrit sur le port série qu'une itération sur 10
    Serial.print("T air: ");
//    Serial.print("Air ");
    Serial.print(temperature1);
//        Serial.print(" CO2 ");
    Serial.print(", T CO2: ");
    Serial.println(temperature2);
   // }

  // set the cursor to column 0, line 1
  // (note: line 1 is the second row, since counting begins with 0):
    lcd.setCursor(0, 0);
    dtostrf(temperature1, 4, 1, temp);
    sprintf(texte, "Air T1 = %s", temp);
    lcd.print(texte);
//    lcd.print("T1 = ");
//    lcd.print(temperature1);
    lcd.write(1);
    lcd.print(F("C "));
    lcd.setCursor(0, 1);
        dtostrf(temperature2, 4, 1, temp);
    sprintf(texte, "CO2 T2 = %s", temp);
    lcd.print(texte);   
        lcd.write(1);
    lcd.print(F("C "));
 
    delay(1000); // On mesure toutes les secondes
}

// utilitaire pour déterminer l'adresse d'un senseur
void discoverOneWireDevices(void) {
  byte i;
  byte present = 0;
  byte data[12];
  byte addr[8];
  
   Serial.begin(9600);
   delay(100);
   #if (DEBUG)
  Serial.print("Looking for 1-Wire devices...\n\r");// "\n\r" is NewLine 
  #endif
  while(oneWire.search(addr)) {
       #if (DEBUG)
    Serial.print("\n\r\n\rFound \'1-Wire\' device with address:\n\r");
    #endif
    for( i = 0; i < 8; i++) {
   #if (DEBUG)      
      Serial.print("0x");
      #endif
      if (addr[i] < 16) {
           #if (DEBUG)
        Serial.print('0');
#endif
      }
      Serial.print(addr[i], HEX);
      if (i < 7) {
           #if (DEBUG)
        Serial.print(", ");
#endif
      }
    }
    if ( OneWire::crc8( addr, 7) != addr[7]) {
         #if (DEBUG)
      Serial.print("CRC is not valid!\n\r");
#endif
      return;
    }
  }
     #if (DEBUG)
  Serial.println();
  Serial.println("Done");
     #endif
  oneWire.reset_search();
  return;
}
