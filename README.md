# Double T


## Hardware
• Arduino uno: https://store.arduino.cc/collections/boards/products/arduino-uno-rev3

• Screw shield: https://www.adafruit.com/product/196

• LCD Shield: https://www.adafruit.com/product/716

• Temperature sensors: https://www.adafruit.com/product/642

## Assembly instructions

• Assemble the screwshield and install it on the Arduino

• Assemble the LCD screen and install it on the screwshield

• Connect the first temperature sensor: GND (black) on GND, VCC (red) on +5V, Signal (yellow) on pin A0. Don't forget the pull-up resistor.

• In the code, uncomment the discoverOneWireDevice(); line comment the #define TEMP_SENSOR1 and #define TEMP_SENSOR2 lines and flash the code on the Arduino. Write down the address of the first sensor (in the form of {0xXX, 0xXX, 0xXX, 0xXX, 0xXX, 0xXX, 0xXX, 0xXX}) 

• Disconnect the first temperature sensor, connect the second one instead, and run the sketch again to determine the address of the second sensor.

• Adapt the #define TEMP_SENSOR1 and #define TEMP_SENSOR2 lines with the addresses determined in the previous steps, and uncomment the two lines

• Comment out the  discoverOneWireDevice(); line

• Upload the sketch on the Arduino

• Enjoy your dual temperature measurement.

## License
Copyright (c) 2022-2023, Jérôme Kasparian

Licenced under the Creative Commons Attribution ShareAlike 4.0, https://creativecommons.org/licenses/by-sa/4.0/

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
